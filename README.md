### Proyectos tomados de Internet
Este Repositorio tiene como finalidad tomar de internet Proyectos resueltos, para 
estudiar la lógica que emplean y como ejercicios diarios para mejorara día a día.
Rafael Aquino. Bogotá diciembre 2022

### Índice de proyectos


>Lista de Proyectos (Cada Proyecto tiene modificaciones realizadas por mi (Rafael Aquino))

---

### 6ProyectosBasicos

>6 Proyectos de Python Básicos - https://youtu.be/tWnyBD2src0
Versión en Español con Modificaciones: Estefania Cassingena Navone (@EstefaniaCassN).
Basado en el proyecto de: Kylie Ying (@kylieyying) para freeCodeCamp en inglés. (https://youtu.be/8ext9G7xspg)

1. project_01 --> Proyecto Historias Locas
2. project_02 --> proyecto Adivina el número - usuario
3. project_03 --> Proyecto Adivina el número - Computadora


---
