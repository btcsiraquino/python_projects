"""
Practicando con proyectos: Bogota,2022/12/12 por Rafael Aquino (https://twitter.com/Rafa_elaquino)
--> Rehago todo el ejercicio con algunas modificaciones
--> Mis líneas de Código comienzan con #-->RA dentro del programa comentado
--> El proyecto original está comentado

Proyecto tomado de:
https://youtu.be/tWnyBD2src0
6 Proyectos de Python Básicos - Curso Completo Paso a Paso
Video creado por Estefania Cassingena Navone (@EstefaniaCassN).
"""


#Algoritmo Modificado
#............................
import random

def adivina_el_número_computadora(x):

    print("="*50)
    print("           ¡Bienvenido(a) al Juego!                       ")
    print("La computadora intentará adivinar el número que escojas.")
    print('El número a adivinar es entre 1 y 10')
    print("="*50)

    numero = int(input(f"Selecciona un número entre 1 y {x} para que la computadora intente adivinarlo."))

    print("Número seleccionado por el usuario: ",numero)
    límite_inferior = 1
    límite_superior = x
    respuesta = ""

    c = 0
    while respuesta != "c":
        # Generar predicción
        if límite_inferior != límite_superior:
            predicción = random.randint(límite_inferior, límite_superior)
        else:
            predicción = límite_inferior  # también podría ser límite_superior porque los límites son iguales.
               
        if predicción == numero:
            print("Predicción de la Computadora: ", predicción)
            print("Listo")
            c += 1
            break
        else:
            print("Predicción de la Computadora: ", predicción)
            c += 1
        
        if predicción > numero:
            límite_superior = predicción - 1
        elif predicción < numero:
            límite_inferior = predicción + 1

    print(f"¡La computadora adivinó tu número {predicción} correctamente en {c} intentos")


adivina_el_número_computadora(10)


"""


Proyecto Básico de Python (Adivina el Número (Computadora)).
Basado en el proyecto de: Kylie Ying (@kylieyying). 
Versión en Español con Modificaciones: Estefania Cassingena Navone (@EstefaniaCassN).


#............................
#Algoritmo Original
#............................

import random

# La computadora debe adivinar el número seleccionado por el jugador.
def adivina_el_número_computadora(x):

    print("============================")
    print("  ¡Bienvenido(a) al Juego!  ")
    print("============================")
    print(f"Selecciona un número entre 1 y {x} para que la computadora intente adivinarlo.")

    # Intervalo de valores válidos
    límite_inferior = 1
    límite_superior = x
    respuesta = ""

    # Miestras el usuario no indique que la respuesta es correcta,
    # continuar el proceso.
    while respuesta != "c":
        # Generar predicción
        if límite_inferior != límite_superior:
            predicción = random.randint(límite_inferior, límite_superior)
        else:
            predicción = límite_inferior  # también podría ser límite_superior porque los límites son iguales.
        
        # Obtener respuesta del usuario
        respuesta = input(f"Mi predicción es {predicción}. Si es muy alta, ingresa (A). Si es muy baja, ingresa (B). Si es correcta ingresa (C) ").lower()
        
        if respuesta == "a":
            límite_superior = predicción - 1
        elif respuesta == "b":
            límite_inferior = predicción + 1

    print(f"¡Siii! La computadora adivinó tu número correctamente: {predicción}")


adivina_el_número_computadora(10)

"""


