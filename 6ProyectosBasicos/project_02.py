"""
Practicando con proyectos: Bogota,2022/12/12 por Rafael Aquino (https://twitter.com/Rafa_elaquino)
--> Rehago todo el ejercicio con algunas modificaciones
--> Mis líneas de Código comienzan con #-->RA dentro del programa comentado
--> El proyecto original está comentado

Proyecto tomado de:
https://youtu.be/tWnyBD2src0
6 Proyectos de Python Básicos - Curso Completo Paso a Paso
Video creado por Estefania Cassingena Navone (@EstefaniaCassN).
"""
#Algoritmo Modificado
#............................
import random
resp = 's'
while (resp == 'S') or (resp == 's'):
    def leer():
        n = int(input('Adivina un número entre 1 y 10: '))
        return n

    def revisa(c,perdiste):
        if c >= 4: 
            perdiste = True
            return perdiste

    print("="*50)
    print("           ¡Bienvenido(a) al Juego!                       ")
    print("Tu meta es adivinar el número generado por la computadora.")
    print('El número a adivinar es entre 1 y 10')
    print("Sólo tienes tres oportunidades")
    print("="*50)

    número_aleatorio = random.randint(1, 10)
    # print(número_aleatorio)
    predicción, c = 0, 0
    perdiste = False
    while (predicción != número_aleatorio) and (c < 3):
        predicción = leer()
        c += 1
        if predicción < número_aleatorio:
            print('Intenta otra vez. Este número es muy pequeño.')
            revisa(c,perdiste)
        elif predicción > número_aleatorio:
            print('Intenta otra vez. Este número es muy grande.')
            revisa(c,perdiste)
        
    if (perdiste == False) and (predicción == número_aleatorio):
        print(f'¡Felicitaciones! Adivinaste el número << {número_aleatorio} >> correctamente.')
        if (c < 2):
            print(f'Con {c} Intento')
        else:
            print(f'Con {c} Intentos')
    else:
        print('El numero era: ', número_aleatorio)
        print('Fin del juego perdiste')
    resp = input('Deseas jugar de nuevo s/n : ')

#............FIN...............................    




"""
Proyecto Básico de Python (Adivina el Número).
Basado en el proyecto de: Kylie Ying (@kylieyying). 
Versión en Español con Modificaciones: Estefania Cassingena Navone (@EstefaniaCassN).




#Algoritmo Original
import random

# El usuario adivina el número aleatorio generado por la computadora.
def adivina_el_número(x):

    print("============================")
    print("  ¡Bienvenido(a) al Juego!  ")
    print("============================")
    print("Tu meta es adivinar el número generado por la computadora.")

    número_aleatorio = random.randint(1, x) # número aleatorio entre 1 y x.

    # La predicción es 0 inicialmente para que no coincida con el número aleatorio. 
    predicción = 0

    # Continuar prediciendo el número hasta que la predicción sea correcta.
    while predicción != número_aleatorio:
        # El usuario ingresa un número.
        predicción = int(input(f'Adivina un número entre 1 y {x}: '))
        # Si el número es menor que el número aleatorio, se 
        # muestra un mensaje.
        if predicción < número_aleatorio:
            print('Intenta otra vez. Este número es muy pequeño.')
        # Si el número es mayor que el número aleatorio, se
        # muestra un mensaje.
        elif predicción > número_aleatorio:
            print('Intenta otra vez. Este número es muy grande.')

    # El ciclo o bucle se detiene cuando el usuario adivina el número
    # correctamente y se muestra un mensaje final.
    print(f'¡Felicitaciones! Adivinaste el número {número_aleatorio} correctamente.')

# Llamar a la función
adivina_el_número(10)

"""
