"""
Practicando con proyectos: Bogota,22/12/08 por Rafael Aquino (https://twitter.com/Rafa_elaquino)
--> Rehago todo el ejercicio con algunas modificaciones
--> Mis líneas de Código comienzan con #-->RA dentro del programa comentado
--> El proyecto original está comentado

Proyecto tomado de:
https://youtu.be/tWnyBD2src0
6 Proyectos de Python Básicos - Curso Completo Paso a Paso
Video creado por Estefania Cassingena Navone (@EstefaniaCassN).
"""

import random
print("-"*40)
print("Mad Libs (Historias Locas)")
print("-"*40)
adj = ['asombroso', 'maravilloso', 'caotico']
verbo1 = ['resolver', 'explicar', 'correr']
verbo2 = ['programar', 'codificar', 'escribir']
s_plural = ['metas', 'objetivos', 'vistas']

print("Tres Historias Locas")
for i in range(3):
    madlib = f"¡Programar es {adj[random.randint(0,2)]}! Siempre me emociona porque me encanta {verbo1[random.randint(0,2)]} problemas. ¡Aprende a {verbo2[random.randint(0,2)]} con freeCodeCamp y alcanza tus {s_plural[random.randint(0,2)]}!"
    print(madlib)
    print("-"*40)


"""
Proyecto Básico de Python (Mad Libs / Historias Locas).
Basado en el proyecto de: Kylie Ying (@kylieyying). 
Versión en Español con Modificaciones: Estefania Cassingena Navone (@EstefaniaCassN).
"""
"""
# Concatenar cadenas de caracteres.
# Supongamos que queremos crear una cadena que diga:
# "Aprende a programar con ________."

organización = "freeCodeCamp" # Cadena de caracteres asignada a una variable.

# Algunas alternativas
print("Aprende a programar con " + organización) # Concatenar
print("Aprende a Programar con", organización) # --> RA
print("Aprende a programar con {}".format(organización)) # Método .format()
print(f"Aprende a programar con {organización}") # f-string

# Mad Libs (Historias Locas)

adj = input("Adjetivo: ") # asombroso
verbo1 = input("Verbo: ") # resolver
verbo2 = input("Verbo: ") # programar
sustantivo_plural = input("Sustantivo (plural): ") # metas, objetivos

madlib = f"¡Programar es {adj}! Siempre me emociona porque me encanta {verbo1} problemas. \
¡Aprende a {verbo2} con freeCodeCamp y alcanza tus {sustantivo_plural}!"

print(madlib)

"""